﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace rppoon
{
    class Note
    {
        private string note;
        private string authorName;
        private int importance;

        public string text
        {
            get { return this.note; }
            set { this.note = value; }
        }
        public string author
        {
            get { return this.note; }
        }

        public int importatnt
        {
            get { return this.importance; }
            set { this.importance = value; }
        }



        public string getNote() { return this.note; }
        public string getAuthorName() { return this.authorName; }
        public int getImportance() { return this.importance; }
        public void setNote(string note) { this.note = note; }
        public void setImportance (int importance) { this.importance = importance; }

        public Note() { this.note = "Empty!"; this.authorName = "Empty!"; this.importance = 0; }
        public Note(String note, String authorName, int importance) { this.note = note; this.authorName = authorName; this.importance = importance; }
        public Note(String note, String authorName) { this.note=note; this.authorName = authorName; }

        public override string ToString()
        {
            return "Bilješka: " + this.note + "\n" + "Autor: " + this.authorName + "\n" + "Vaznost: " + this.importance;
        }
    }
}
